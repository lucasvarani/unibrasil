<footer class="footer blue-gradient-bg py-4">

	<div class="container">
		<div class="row text-center text-md-left">
			<div class="col-md-3 text-white">
				<strong>Sobre nós</strong>
				<a href="#" class="d-block my-4"><img src="images/logo.png"></a>
				<p>Nossa Missão é oferecer Educação Superior contemporânea comprometida com a formação profissional qualificada para o mercado de trabalho.</p>
				<div class="social-buttons text-center mt-4 mb-4 mb-md-0">
					<a href="#" class="social-button face-btn border-light rounded-circle mr-2"><i class="fab fa-facebook-f text-white"></i></a>	
					<a href="#" class="social-button insta-btn border-light rounded-circle mr-2"><i class="fab fa-instagram text-white"></i></a>	
					<a href="#" class="social-button in-btn border-light rounded-circle"><i class="fab fa-linkedin-in text-white"></i></a>			
				</div>
			</div>
			<div class="col-md-4 offset-md-1 text-white">
				<strong>Mapa do site</strong>
				<div class="row mt-4">
					<div class="col-sm-6 p-0">
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
					</div>
					<div class="col-sm-6">
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
						<a href="#" class="nav-link text-white">Nonononnonononn</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 offset-md-1 text-white">
				<strong>Contato</strong>
				<p class="mt-4">Campus São Paulo - Rua Carolina Fonseca, 584
				<br>Itaquera • São Paulo/SP</p>
				<p>Campus Descalvado - Av. Hilário da Silva Passos, 950
				<br>Parque Universitário • Descalvado/SP</p>
				<p>Campus Fernandópolis - Est. Projetada F-1, s/n
				<br>Fazenda Santa Rita • Fernandópolis/SP</p>
			</div>
		</div>
	</div>

	<!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <!-- Slick JS -->
  <script src="js/slick/slick.js"></script>
  <!-- Functions -->
  <script src="js/functions.js"></script> 

</footer