<section class="navbar-section fixed-top">
  <!-- DESKTOP LINKS -->
  <nav class="navbar link-navbar secondary-blue-bg navbar-expand-lg d-none d-lg-block py-0">
    <div class="container-fluid">
      <div class="row w-100">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item py-2">
            <a class="nav-link text-white border-lg-right px-3 py-1" href="#">Portal do aluno</a>
          </li>
          <li class="nav-item py-2">
            <a class="nav-link text-white border-lg-right px-3 py-1" href="#">Portal do professor</a>
          </li>
          <li class="nav-item py-2">
            <a class="nav-link text-white px-3 py-1" href="#">Seja nossa parceiro</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item py-2">
            <a class="nav-link text-white border-lg-right px-3 py-1" href="#">Institucional</a>
          </li>
          <li class="nav-item py-2">
            <a class="nav-link text-white border-lg-right px-3 py-1" href="#">Unidades</a>
          </li>
          <li class="nav-item py-2">
            <a class="nav-link text-white px-3 py-1" href="#">Loja</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- END DESKTOP LINKS -->

  <nav class="navbar navbar-expand-lg py-0 primary-navbar primary-blue-bg px-0 px-lg-3">
    <a class="ml-md-5 pl-md-5 navbar-brand pl-3 px-lg-0 pt-3 pt-lg-0 mb-2 mb-lg-0" href="#"><img src="images/logo.png"></a>
    <button class="navbar-toggler pr-3 open-menu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <!-- <i class="fas fa-bars text-white"></i> -->
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <!-- DESKTOP MENU -->
      <ul class="navbar-nav ml-auto d-none d-lg-flex p-relative navbar-nav-desktop">
        <li class="nav-item py-3 p-relative">
          <a class="nav-link text-white border-lg-right px-4" href="#">Vestibular</a>
        </li>
        <div class="dropdownhover">
          <li class="nav-item py-3 dropdown">
            <a class="nav-link text-white border-lg-right px-4" href="#">Graduação</a>
          </li>        
          <div class="dropdown-content align-items-center">
            <div class="drop-item py-2">
               <a class="text-white border-lg-right px-3 py-1" href="#">Conheça os cursos</a>
            </div>
            <div class="drop-item py-2">
              <a class="text-white border-lg-right px-3 py-1" href="#">Inscreva-se no Vestibular</a>
            </div>
            <div class="drop-item py-2">
              <a class="text-white border-lg-right px-3 py-3" href="#">Transferência</a>
            </div>
            <div class="drop-item py-2">
              <a class="text-white px-3 py-1" href="#">Segunda Graduação</a>
            </div>
          </div>
        </div>
        <div class="dropdownhover">
          <li class="nav-item py-3 dropdown">
            <a class="nav-link text-white border-lg-right px-4" href="#">Pós-Graduação</a>
          </li>     
          <div class="dropdown-content align-items-center ">
            <div class="drop-item py-3">
              <a class="text-white border-lg-right px-3 py-1" href="#">Especialização</a>
            </div>
            <div class="drop-item py-3">
              <a class="text-white border-lg-right px-3 py-1" href="#">Mestrado</a>
            </div>
            <div class="drop-item py-3">
              <a class="text-white px-3 py-1" href="#">Doutorado</a>
            </div>
          </div>   
        </div>
        <li class="nav-item py-3 p-relative">
          <a class="nav-link text-white border-lg-right px-4" href="#">EAD</a>
        </li>
        <li class="nav-item py-3 p-relative">
          <a class="nav-link text-white border-lg-right px-4" href="#">Bolsas</a>
        </li>
        <div class="dropdownhover">
          <li class="nav-item py-3 dropdown">
            <a class="nav-link text-white px-4" href="#">Fale Conosco</a>
          </li>
          <div class="dropdown-content align-items-center">
            <div class="drop-item py-3 pr-5">
              <a class="text-white px-3 py-1" href="#">Ouvidoria</a>
            </div>
          </div>
        </div>
      </ul>
      <!-- END DESKTOP MENU -->

      <!-- MOBILE MENU -->
        <ul class="links-nav nav nav-justified py-2 d-lg-none d-flex">
          <li class="nav-item border-right">
            <a class="nav-link text-white" href="#!">Portal do aluno</a>
          </li>
          <li class="nav-item border-right">
            <a class="nav-link text-white" href="#!">Portal do professor</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="#!">Seja nossa parceiro</a>
          </li>
        </ul>
        <ul class="navbar-nav d-lg-none d-flex p-relative navbar-nav-mobile">
          <li class="nav-item">
            <a href="#" class="nav-link text-white px-3">Vestibular</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white px-3" data-toggle="dropdown"
               href="#!" role="button" aria-haspopup="true" aria-expanded="false">
               Graduação</a>
            <div class="dropdown-menu px-3">
              <a class="dropdown-item text-white" href="#!">Conheça os cursos</a>
              <a class="dropdown-item text-white" href="#!">Inscreva-se no vestibular</a>
              <a class="dropdown-item text-white" href="#!">Transferência</a>
              <a class="dropdown-item text-white" href="#!">Segunda graduação</a>
            </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle text-white px-3" data-toggle="dropdown"
                 href="#!" role="button" aria-haspopup="true" aria-expanded="false">
                 Pós-Graduação</a>
              <div class="dropdown-menu px-3">
                <a class="dropdown-item text-white" href="#!">Especialização</a>
                <a class="dropdown-item text-white" href="#!">Mestrado</a>
                <a class="dropdown-item text-white" href="#!">Doutorado</a>
              </div>
            </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-white px-3">EAD</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-white px-3">Bolsas</a>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle text-white px-3" data-toggle="dropdown"
                 href="#!" role="button" aria-haspopup="true" aria-expanded="false">Fale Conosco</a>
              <div class="dropdown-menu px-3">
                <a class="dropdown-item text-white" href="#!">Ouvidoria</a>
              </div>
            </li>
        </ul>
        <ul class="links-nav nav nav-justified py-2 d-lg-none d-flex">
          <li class="nav-item border-right">
            <a class="nav-link text-white" href="#!">Institucional</a>
          </li>
          <li class="nav-item border-right">
            <a class="nav-link text-white" href="#!">Unidades</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="#!">Loja</a>
          </li>
        </ul>
      <!-- END MOBILE MENU -->

    </div>
  </nav>
</section>