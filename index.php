<!DOCTYPE html>
<html lang="en">
  
  <!-- HEAD -->
  <?php include 'includes/head.php' ?>

  <body>   

    <!-- MENU -->
    <?php include 'includes/menu.php' ?>

    <!-- CONTENT -->

    <!-- BANNER -->
    <section class="banner-section">
      <div id="banner_principal" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#banner_principal" data-slide-to="0" class="active"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active">
            <img class="d-block w-100" src="images/banner_home/banner-universidade-brasil.jpg" data-src="holder.js/900x400?theme=social" alt="First slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#banner_principal" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#banner_principal" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </section>      
    <!-- END BANNER -->

    <!-- HIGHLIGHTS -->
    <section class="section-destaques secondary-blue-bg my-5 py-4">
      <div class="container">
        <h2 class="text-white">Destaques</h2>
        <div class="row py-4">
          <div class="col-lg-4 col-sm-6 mb-5">
            <div class="px-4">
              <a href="#!" class="blue-highlight-box text-white text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex1.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <div class="highlight-box-button text-white m-auto">Ver mais</div>
              </a>
            </div>       
          </div>
          <div class="col-lg-4 col-sm-6 mb-5">
            <div class="px-4">
              <a href="#!" class="blue-highlight-box text-white text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex2.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <div class="highlight-box-button text-white m-auto">Ver mais</div>
              </a>
            </div>       
          </div>
          <div class="col-lg-4 col-sm-6 mb-5">
            <div class="px-4">
              <a href="#!" class="blue-highlight-box text-white text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex3.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <div class="highlight-box-button text-white m-auto">Ver mais</div>
              </a>
            </div>       
          </div>
          <div class="col-lg-4 col-sm-6 mb-5">
            <div class="px-4">
              <a href="#!" class="blue-highlight-box text-white text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex4.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <div class="highlight-box-button text-white m-auto">Ver mais</div>
              </a>
            </div>       
          </div>
          <div class="col-lg-4 col-sm-6 mb-5">
            <div class="px-4">
              <a href="#!" class="blue-highlight-box text-white text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex5.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <div class="highlight-box-button text-white m-auto">Ver mais</div>
              </a>
            </div>       
          </div>
          <div class="col-lg-4 col-sm-6 mb-5">
            <div class="px-4">
              <a href="#!" class="blue-highlight-box text-white text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex6.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <div class="highlight-box-button text-white m-auto">Ver mais</div>
              </a>
            </div>       
          </div>
        </div>
      </div>      
    </section>
    <!-- END HIGHLIGHTS -->

    <section class="conheca-unidades-section text-center text-white mb-5">
      <h2 class="display-4">Conheça<br><strong>Nossas unidades do Grupo</strong></h2>
      <a href="#!" class="banner-btn text-white">Clique aqui</a>
    </section>

    <!-- NEWS HIGHLIGHTS -->
    <section class="news-highlights-section my-5 py-4">
      <div class="container">
        <h2 class="blue-font-color">Acontece na Universidade</h2>
        <div class="row px-5 mt-4">
          <div class="news-highlights-carousel w-100">
            <div class="p-3">
              <div class="white-highlight-box text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex1.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <a href="#!" class="highlight-box-button text-white m-auto">Ver mais</a>
              </div>
            </div>
            <div class="p-3">
              <div class="white-highlight-box text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex2.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <a href="#!" class="highlight-box-button text-white m-auto">Ver mais</a>
              </div>
            </div>
            <div class="p-3">
              <div class="white-highlight-box text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex3.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <a href="#!" class="highlight-box-button text-white m-auto">Ver mais</a>
              </div>
            </div>
            <div class="p-3">
              <div class="white-highlight-box text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex4.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <a href="#!" class="highlight-box-button text-white m-auto">Ver mais</a>
              </div>
            </div>
            <div class="p-3">
              <div class="white-highlight-box text-center p-relative d-block highlight-box">
                <div class="highlight-box-img">                
                  <img src="images/img-ex5.jpg">
                </div>
                <div class="px-4 pt-3">
                  <strong class="highlight-box-title">Nononononno</strong>
                  <p class="font-weight-light my-3">
                    Lorem ipsum dolor sit amet, consectetuer adipi
                  </p>
                </div>                
                <a href="#!" class="highlight-box-button text-white m-auto">Ver mais</a>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </section>
    <!-- END NEWS HIGHLIGHTS -->

    <section class="interest-section my-4 py-4">
      <div class="container">
        <h2 class="blue-font-color">Áreas de Interesse</h2>
      </div>
      <div class="container-fluid p-0">
        <div class="row text-center w-100 mx-0 my-3">
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/arquitetura.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Arquitetura, Arte,<br> Design e Moda</span>
              </div>              
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/comunicacao.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Comunicação</span>
              </div>     
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/direito.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Direito</span>
              </div>  
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/educacao.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Educação</span>
              </div>  
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/engenharia.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Engenharia e Tecnologia</span>
              </div>  
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/negocios.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Negócios</span>
              </div>  
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/saude.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Saúde</span>
              </div>  
            </a>
          </div>
          <div class="col-lg-3 col-sm-6 text-center p-0">
            <a href="#!" class="p-relative d-block text-white interest-box">
              <img class="w-100 interest-img" src="images/turismo.jpg">
              <div class="p-relative interest-box-title d-flex">
                <span class="m-auto">Turismo e Hospitalidade</span>
              </div>  
            </a>
          </div>
        </div>
        <div class="row w-100 mx-0 mt-5">          
          <a href="#!" class="hvr-rectangle-out text-shadow-pop-bottom subscribe-btn text-white font-weight-bold secondary-blue-bg mx-auto px-4">Inscreva-se</a>
        </div>
      </div>
    </section>

    <!-- END CONTENT -->

    <!-- FOOTER -->
    <?php include 'includes/footer.php' ?>

  </body>

</html>