// jQuery animação botão menu mobile 
$(function() {                       //run when the DOM is ready
  $(".open-menu").click(function() {  //use a class, since your ID gets mangled
  	if ($(this).hasClass("open")) {
  		$(this).removeClass("open");		//remove class if exist
  	}else{
    	$(this).addClass("open");      //add the class to the clicked element  	  		
  	}
  });
  $(".navbar-nav-mobile .dropdown-toggle").click(function() {  //use a class, since your ID gets mangled
  	if ($(this).hasClass("open")) {
  		$(this).removeClass("open");		//remove class if exist
  	}else{
    	$(this).addClass("open");      //add the class to the clicked element  	  		
  	}
  });
});

// Verifica se é mobile
var isMobile = false; //initiate as false
// screen detection
if (screen.width<=991) {
 isMobile = true
}

if (isMobile) {
  $("body").addClass("mt-lg-down");        
}else{
  $("body").addClass("mt-lg-up");
}      

// Função de scroll menu
$(window).scroll(function() {    
  var scroll = $(window).scrollTop();

  if (scroll >= 40) {
    $(".link-navbar .nav-item").removeClass("py-2");
    $(".link-navbar .nav-item").addClass("py-1");
    $(".navbar-nav-desktop .nav-item").removeClass("py-3");
    $(".navbar-nav-desktop .nav-item").addClass("py-2");
  } else {
    $(".link-navbar .nav-item").addClass("py-2");
    $(".link-navbar .nav-item").removeClass("py-1");
    $(".navbar-nav-desktop .nav-item").addClass("py-3");
    $(".navbar-nav-desktop .nav-item").removeClass("py-2");
  }
});

// SLICK CAROUSEL JQUERY
$(document).ready(function(){
  $('.news-highlights-carousel').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }
    ]
  });
});